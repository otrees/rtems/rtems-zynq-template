/*  Init
 *
 *  This routine is the initialization task for this test program.
 *  It is called from init_exec and has the responsibility for creating
 *  and starting the tasks that make up the test.  If the time of day
 *  clock is required for the test, it should also be set to a known
 *  value by this function.
 *
 *  Input parameters:  NONE
 *
 *  Output parameters:  NONE
 *
 *  COPYRIGHT (c) 1989-1999.
 *  On-Line Applications Research Corporation (OAR).
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.rtems.com/license/LICENSE.
 *
 *  $Id: init.c,v 1.12.4.1 2003/09/04 18:46:30 joel Exp $
 */

#define CONFIGURE_INIT
#include "app_def.h"
#include "system.h"
#include <fcntl.h>
#include <machine/rtems-bsd-config.h>
#include <rtems/dhcpcd.h>
#include <rtems/error.h>
#include <rtems/monitor.h>
#include <rtems/shell.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>

#define DEFAULT_NETWORK_DHCPCD_ENABLE

#if defined(DEFAULT_NETWORK_DHCPCD_ENABLE) &&                                  \
    !defined(DEFAULT_NETWORK_NO_STATIC_IFCONFIG)
#define DEFAULT_NETWORK_NO_STATIC_IFCONFIG
#endif

#include <assert.h>
#include <sysexits.h>

#include <rtems/bsd/bsd.h>
#include <rtems/rtems_bsdnet.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define CONFIGURE_SHELL_COMMANDS_INIT
#define CONFIGURE_SHELL_COMMANDS_ALL
#define CONFIGURE_SHELL_MOUNT_MSDOS

sem_t sem1;

#include <bsp/irq-info.h>
#include <rtems/netcmds-config.h>

#ifdef RTEMS_BSD_MODULE_USER_SPACE_WLANSTATS
#define SHELL_WLANSTATS_COMMAND &rtems_shell_WLANSTATS_Command,
#else
#define SHELL_WLANSTATS_COMMAND
#endif

#ifdef RTEMS_BSD_MODULE_USR_SBIN_WPA_SUPPLICANT
#define SHELL_WPA_SUPPLICANT_COMMAND &rtems_shell_WPA_SUPPLICANT_Command,
#else
#define SHELL_WPA_SUPPLICANT_COMMAND
#endif

#ifdef SHELL_TTCP_COMMAND_ENABLE
#define SHELL_TTCP_COMMAND &rtems_shell_TTCP_Command,
#else
#define SHELL_TTCP_COMMAND
#endif

#define CONFIGURE_SHELL_USER_COMMANDS                                          \
  SHELL_WLANSTATS_COMMAND                                                      \
  SHELL_WPA_SUPPLICANT_COMMAND                                                 \
  SHELL_TTCP_COMMAND                                                           \
  &bsp_interrupt_shell_command, &rtems_shell_ARP_Command,                      \
      &rtems_shell_HOSTNAME_Command, &rtems_shell_PING_Command,                \
      &rtems_shell_ROUTE_Command, &rtems_shell_NETSTAT_Command,                \
      &rtems_shell_IFCONFIG_Command, &rtems_shell_TCPDUMP_Command,             \
      &rtems_shell_SYSCTL_Command, &rtems_shell_VMSTAT_Command

#include <rtems/shellconfig.h>

#define BUILD_VERSION_STRING(major, minor, patch)                              \
  __XSTRING(major) "." __XSTRING(minor) "." __XSTRING(patch)

void bad_rtems_status(rtems_status_code status, int fail_level,
                      const char *text) {
  printf("ERROR: %s status %s", text, rtems_status_text(status));
  status = rtems_task_delete(RTEMS_SELF);
}

int testcmd_forshell(int argc, char **argv) {
  int i;
  printf("Command %s called\n", argv[0]);
  for (i = 1; i < argc; i++)
    if (argv[i])
      printf("%s", argv[i]);
  printf("\n");
  return 0;
}

static void default_network_dhcpcd(void) {
#ifdef DEFAULT_NETWORK_DHCPCD_ENABLE
  static const char default_cfg[] = "clientid libbsd test client\n";
  rtems_status_code sc;
  int fd;
  int rv;
  ssize_t n;

  fd =
      open("/etc/dhcpcd.conf", O_CREAT | O_WRONLY, S_IRWXU | S_IRWXG | S_IRWXO);
  assert(fd >= 0);

  n = write(fd, default_cfg, sizeof(default_cfg) - 1);
  assert(n == (ssize_t)sizeof(default_cfg) - 1);

#ifdef DEFAULT_NETWORK_DHCPCD_NO_DHCP_DISCOVERY
  static const char nodhcp_cfg[] = "nodhcp\nnodhcp6\n";

  n = write(fd, nodhcp_cfg, sizeof(nodhcp_cfg) - 1);
  assert(n == (ssize_t)sizeof(nodhcp_cfg) - 1);
#endif

  rv = close(fd);
  assert(rv == 0);

  sc = rtems_dhcpcd_start(NULL);
  assert(sc == RTEMS_SUCCESSFUL);
#endif
}

int network_init(void) {
  rtems_status_code sc;

  sc = rtems_bsd_initialize();
  if (sc != RTEMS_SUCCESSFUL) {
    printf("rtems_bsd_initialize failed\n");
    return -1;
  }

  /* Let the callout timer allocate its resources */

  sc = rtems_task_wake_after(2);
  if (sc != RTEMS_SUCCESSFUL) {
    printf("rtems_task_wake_after failed\n");
    return -1;
  }

  default_network_dhcpcd();

  return 0;
}

rtems_task Init(rtems_task_argument ignored) {
  rtems_status_code status;

  printf("\n\nRTEMS v " BUILD_VERSION_STRING(__RTEMS_MAJOR__, __RTEMS_MINOR__,
                                             __RTEMS_REVISION__) "\n");

  if (network_init() == -1) {
    printf("rtems_bsd_ifconfig_lo0 failed\n");
  };

  rtems_monitor_init(RTEMS_MONITOR_SUSPEND | RTEMS_MONITOR_GLOBAL);
  /*rtems_capture_cli_init (0);*/

  printf("Starting application " APP_VER_ID " v " BUILD_VERSION_STRING(
      SW_VER_MAJOR, SW_VER_MINOR, SW_VER_PATCH) "\n");

  rtems_shell_init("SHLL", RTEMS_MINIMUM_STACK_SIZE + 0x1000,
                   SHELL_TASK_PRIORITY, "/dev/console", 1, 0, NULL);

  rtems_shell_add_cmd("zynq_apfoo", "app", "zynq_apfoo", zynq_apfoo);

  status = rtems_task_delete(RTEMS_SELF);

  printf("*** END OF TEST2 ***\n");
  exit(0);
}
