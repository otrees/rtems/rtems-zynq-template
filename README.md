# RTEMS Zynq mzAPO board template

This directory contains RTEMS template application for mzAPO board.

Compilation stepts:
export PATH=$PATH:/opt/rtems/6/bin
make

RTEMS compilation at https://gitlab.fel.cvut.cz/otrees/rtems/work-and-ideas/-/wikis/home
